# SVXREFLECTOR #
The svxreflector server is used to interconnect multiple SvxLink nodes into one network. All audio sent to the reflector from one node is retransmitted on all other connected nodes. Only one talker is allowed at a time so if another node starts sending audio when another one is already active, the second node will not interrupt the first talker.

# INSTALL DOCKER AND DOCKER-COMPOSE #
follow the official guides

- [install docker](https://docs.docker.com/engine/install/)
- [install docker-compose](https://docs.docker.com/compose/install/)

# BUILD AND RUN SVXREFLECTOR #

```console
$ git clone https://gitlab.com/VincenzoMart/svxreflector-docker.git 
$ cd svxreflector-docker
```
edit svxreflector.conf to suit your needs 
and run svxreflector with the following command.
```console
$ docker-compose up -d
```

# VIEW CONSOLE OUTPUT OF SVXREFLECTOR #
```console
$ docker exec -it svxreflector  screen -x svxreflector
```
to detach from console use ctrl+a followed by d.
