#!/usr/bin/env bash

sudo useradd -d /home/svxlink -m -s /sbin/nologin svxlink

cd /home/svxlink

git clone https://github.com/sm0svx/svxlink.git

cd svxlink/src 

mkdir build && cd build

cmake -DUSE_QT=OFF -DCMAKE_INSTALL_PREFIX=/usr \
-DSYSCONF_INSTALL_DIR=/etc -DLOCAL_STATE_DIR=/var -DCMAKE_BUILD_TYPE=Release .. && make

sudo make install 