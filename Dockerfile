FROM ubuntu:18.04

LABEL maintainer="Vincenzo Marturano"

# install dependencies and other packages
RUN apt-get update &&\
DEBIAN_FRONTEND=noninteractive apt-get install -y g++ cmake make libsigc++-2.0-dev libgsm1-dev libpopt-dev tcl-dev \
libgcrypt20-dev libspeex-dev libasound2-dev libopus-dev librtlsdr-dev libqt4-dev libjsoncpp-dev \
doxygen groff alsa-utils vorbis-tools curl libcurl4-openssl-dev git sudo screen

#build and install svxlink
COPY svxInstall.sh /svxInstall.sh
RUN  chmod +x /svxInstall.sh
RUN /svxInstall.sh 

#Start svxreflector
ENTRYPOINT screen -S 'svxreflector' -dm sudo -u svxlink svxreflector && screen -x svxreflector
